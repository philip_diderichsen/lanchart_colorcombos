# LANCHART colorcombos

Generer farvekoder for hver mulig talerkode på tre bogstaver i Sprogforandringscentrets talesprogskorpus - LANCHART-korpusset.

Udviklet til at give noget visuel identitet til talerne i korpusset.

Koden genererer en TSV-fil med farvekoder og en HTML-fil med en farverendering af talerkoderne som fx kan printes som en plakat.


## Taler- og farvekoder

De genererede talerkoder er samtlige kombinationer (med tilbagelægning) af bogstaverne ABCDEFGHIJKLMNOPQRSTUVWXYZ (plus cifrene 0-9 for de talerkoder der begynder med S eller U). Det giver i alt 18816 forskellige talerkoder.

(Hvorfor inkluderes tallene i koder på S- og U-? Der er en del talerkoder af typen S01, S02 osv. Det er stemmeprøver. Derudover er ukendte talere kodet som UK1, UK2, U10 osv.)

Talerkoderne kobles med hver sin tilfældige, unikke farvekombination med tre komponenter: en baggrundsfarve, en rammefarve og en tekstfarve. Hver (mulig) talerkode i LANCHART-korpusset får således en individuel farvekode i form af en unik farvet 'brik'.

Interviewerne i LANCHART-korpusset har pr. konvention talerkoder der begynder med "X". Disse interviewerkoder får grå farvekombinationer (et signal om at de i en vis forstand er 'sekundære' i samtalen'), mens resten af koderne er mere spraglede. 


## HTML-rendering (plakat)

Koden genererer også en HTML-rendering med alle talerkodernes farvebrikker sat op i et rektangel.

Plakaten "LANCHART-talerne" indeholder kun talerkoder uden tal.

Som udgangspunkt sættes brikkerne op i det format der kommer tættest på 1:2 (en bred til to høj). Disse "anbefalede dimensioner" skrives til terminalen. De kan tilpasses vha. parameteren `custom_xwidth`, der specificerer hvor mange brikker der skal være på en række.

HTML-renderingen er beregnet til at blive skrevet ud som en plakat/PDF. Det er dog ikke helt ligetil. På min Mac har jeg gjort det som følger.

- Åben HTML-renderingen i Firefox.
- Vælg Print. OBS: Hvis udskriften er i et format der gør at udskriften overskrider en side, vil Firefox prompte crashe. Det fikses ved at indstille "Scale" i printindstillingerne til noget mindre så udskriften kun fylder en side.
- Vælg den `custom_xwidth` i koden og "Scale" i printindstillingerne der giver det bedste layout.
- Gem som PDF.



## Projektstruktur

```
lanchart_colorcombos/
|-- README.md
|-- make_colorcombos.py
|-- output
|   |-- colorcombos.html
|   `-- colorcombos.tsv
|-- requirements.txt
`-- venv
    |-- ...
```
## Systemkrav

- Python 3.9 eller højere.
- Python-requirements (requirements.txt).


## Kør

1. Start virtual environment (`python3 -m venv venv ; source ./venv/bin/activate`)
2. Opdater pip og installer requirements (`pip install --upgrade pip ; pip install -r requirements.txt`) (Hvis pip ikke virker, så prøv pip3)
3. Kør make_colorcombos.py. Output skrives til output/.


## Se også

- https://bitbucket.org/philip_diderichsen/parse_textgrids (Transformering LANCHART-korpusset fra TextGrids til Corpus Workbench-format).
- https://bitbucket.org/philip_diderichsen/lanchart_partitur (Partiturvisning der bruger farvekoderne).
