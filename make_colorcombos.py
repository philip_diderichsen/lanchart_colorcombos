import itertools
import random
import re

import lxml
import math
from lxml.html import builder as elem

STYLE = '''
        @page {
            size: auto;   /* auto is the initial value */
            /* this affects the margin in the printer settings */
            margin-left: 12mm;
            margin-right: 10mm;
            }
        body {
            /* this affects the margin on the content before sending to printer */
            margin: 10px;
            font-family: arial;
            }
        h1 {
            font-family: arial;
            font-size: 15em;
            margin-top: 20mm;
            margin-bottom: 10mm;
            letter-spacing: -8;
            }
        p {
            font-family: arial;
            font-size: 3em;
            }
        .row {
            display: table-row;
            }
        .cell {
            display: table-cell;
            padding: 4px;
            font-size: .7em;
            font-weight: bold;
            }
        .inner {
            border: 2px solid grey;
            padding: 1px;
            text-align: center;
            border-radius: 2px;
            margin: 1px;
            }
'''

# ALPHA52: R's "pals" package's alphabet() and alphabet2() combined.
# (Including greys: "#808080", "#191919", "#565656", "#E2E2E2").
ALPHA52 = ["#F0A0FF", "#0075DC", "#993F00", "#4C005C", "#005C31", "#2BCE48", "#FFCC99",
           "#94FFB5", "#8F7C00", "#9DCC00", "#C20088", "#003380", "#FFA405", "#FFA8BB",
           "#426600", "#FF0010", "#5EF1F2", "#00998F", "#E0FF66", "#740AFF", "#990000",
           "#FFFF80", "#FFE100", "#FF5005", "#AA0DFE", "#3283FE", "#85660D", "#782AB6",
           "#1C8356", "#16FF32", "#F7E1A0", "#1CBE4F", "#C4451C", "#DEA0FD", "#FE00FA",
           "#325A9B", "#FEAF16", "#F8A19F", "#90AD1C", "#F6222E", "#1CFFCE", "#2ED9FF",
           "#B10DA1", "#C075A6", "#FC1CBF", "#B00068", "#FBE426", "#FA0087", "#808080",
           "#191919", "#565656", "#E2E2E2"]
GREYBG = ['#0D0D0D', '#2F2F2F', '#404040', '#4C4C4C', '#575757', '#606060', '#686868',
          '#6F6F6F', '#767676', '#7D7D7D', '#838383', '#898989', '#8E8E8E', '#F2F2F2',
          '#EFEFEF', '#EDEDED', '#EAEAEA', '#E7E7E7', '#E4E4E4', '#E1E1E1', '#DDDDDD',
          '#DADADA', '#D7D7D7', '#D4D4D4', '#D0D0D0', '#CDCDCD']
GREYBOW = ['#4D4D4D', '#8B8B8B', '#B3B3B3', '#805959', '#788059', '#598069', '#596980',
           '#785980', '#998282', '#949982', '#82998B', '#828B99', '#948299']
FGDICT = {'light': {"red": "#FAC7D3", "orange": "#FCD2C5", "yellow": "#FFEDC2", "yellowgreen": "#EAF0D0",
                    "bluegreen": "#C3FDEE", "blue": "#E6E6FF", "neutral1": "whitesmoke", "neutral2": "#CFCFCF"},
          'dark': {"red": "#4B0717", "orange": "#742006", "yellow": "#664700", "yellowgreen": "#515C1E",
                   "bluegreen": "#024F3B", "blue": "#052B38", "neutral1": "black", "neutral2": "#493939"}}

RED_GREEN_BLUE_LUMINANCE = (299, 587, 114)


def make_colorcombos(backgroundcols, bordercols, fgcols, fgdict, rand=True):
    """Lav farvekombinationer i farver til informanter (dvs. ikke interviewere)."""
    combos = []
    for bg in backgroundcols:
        for border in bordercols:
            for color in fgcols:
                lightness = get_fg_lightness(hexcol2ints(bg))
                if lightness == 'light':
                    adjusted_bg = darken_hexcol(bg, 5)
                else:
                    adjusted_bg = lighten_hexcol(bg, 20)
                fg = fgdict[lightness][color]
                combos.append({'bg': adjusted_bg, 'border': border, 'fg': fg})

    if rand:
        random.seed(1)
        random.shuffle(combos)
    else:
        luminance_sorted_bg = enumerate(sort_by_luminance([x['bg'] for x in combos]))
        order = {v: i for i, v in luminance_sorted_bg}
        combos = sorted(combos, key=lambda x: order[x['bg']])
    return combos


def hexcol2ints(hexcol):
    """Returner farvekomponenterne fra en hex-farvekode (fx #4B0717) som heltal."""
    return [int(i, 16) for i in [hexcol[1:3], hexcol[3:5], hexcol[5:7]]]


def get_fg_lightness(rgb):
    """Returner hvad skriftfarven skal være ift. en given baggrundsfarve (light/dark)."""
    # Tak til http://blog.nitriq.com/BlackVsWhiteText.aspx
    # Perceived luminance, calculated from http://en.wikipedia.org/wiki/Luminance(video)
    red_lum, green_lum, blue_lum = RED_GREEN_BLUE_LUMINANCE
    max_lum = red_lum * 255 + green_lum * 255 + blue_lum * 255
    mid_lum = max_lum / 2
    total_custom_lum = rgb[0] * red_lum + rgb[1] * green_lum + rgb[2] * blue_lum
    return 'light' if total_custom_lum <= mid_lum else 'dark'


def sort_by_luminance(hexcolors):
    """Returner liste af farver sorteret efter luminans."""
    r_lum, g_lum, b_lum = RED_GREEN_BLUE_LUMINANCE
    lum_dict = dict()
    for hexcol in hexcolors:
        rgb_ints = hexcol2ints(hexcol)
        luminance = rgb_ints[0] * r_lum + rgb_ints[1] * g_lum + rgb_ints[2] * b_lum
        lum_dict[hexcol] = luminance
    sorted_luminances = sorted([lum for lum in lum_dict.values()])
    order = {v: i for i, v in enumerate(sorted_luminances)}
    sorted_hexcolors = sorted(hexcolors, key=lambda x: order[lum_dict[x]])
    return sorted_hexcolors


def make_colcombo_tsv(filename):
    """Kobl alle mulige talerkoder til farvekombinationer, og skriv dem til fil."""
    letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    lettercombos = [''.join(comb) for comb in itertools.product(letters, repeat=3)]
    # Only pure letter combos - except if the combo starts with S or U (reserved for special 'speakers')
    lettercombos = [combo for combo in lettercombos if re.match(r'[A-Z]+$', combo) or combo[0] in 'SU']
    xcombos = [combo for combo in lettercombos if combo.startswith('X')]
    fgdict = make_fg_dict()

    # [:48]: Vi dropper de grå baggrundsfarver for at reservere dem til interviewere.
    colorcombos = make_colorcombos(backgroundcols=[x for x in ALPHA52[:48] if x != '#C075A6'],
                                   bordercols=ALPHA52,
                                   fgcols=fgdict['dark'],
                                   fgdict=fgdict, rand=True)
    greycombos = make_colorcombos(backgroundcols=GREYBG,
                                  bordercols=GREYBOW,
                                  fgcols=['neutral1', 'neutral2'],
                                  fgdict=FGDICT)
    print(f'Antal bogstavkombinationer: {len(lettercombos)}. Heraf begyndende med "X": {len(xcombos)}.')
    print(f'Antal farvede kombinationer: {len(colorcombos)}. Antal grå kombinationer: {len(greycombos)}.')

    with open(filename, 'w', encoding='utf8') as outfile:
        outfile.write('code\tbg\tborder\tfg\n')
        for speakercode in lettercombos:
            if speakercode.startswith('X'):
                combo = code2colorcombo(speakercode, xcombos, greycombos, letters)
            else:
                combo = code2colorcombo(speakercode, lettercombos, colorcombos, letters)
            combo_str = '\t'.join(combo.values())
            outfile.write(f"{speakercode}\t{combo_str}\n")


def make_fg_dict():
    """Lav dict med lyse og mørke farver."""
    luminance_sorted = sort_by_luminance(ALPHA52)
    # Farver oprindelig fra https://mokole.com/palette.html
    colors = '#d2b4ac #ff4500 #ffd700 #00fa9a #0000ff #1e90ff #c71585 #888888'.split()
    lightened = [lighten_hexcol(x, 75) for x in colors]
    darkened = [darken_hexcol(x, 50) for x in colors]
    light = {f'c{i}': x for i, x in enumerate(lightened)}
    dark = {f'c{i}': x for i, x in enumerate(darkened)}
    return {'light': light, 'dark': dark}


def lighten_hexcol(hexcol, percent=50):
    """Gør en hex-farvekode lysere."""
    rgb_ints = hexcol2ints(hexcol)
    headroom = [255 - i for i in rgb_ints]
    added_light = [int(i * percent / 100) for i in headroom]
    lighter_ints = [x[0] + x[1] for x in zip(rgb_ints, added_light)]
    lighter_hexcol = '#' + ''.join([hex(i)[2:].zfill(2) for i in lighter_ints])
    return lighter_hexcol


def darken_hexcol(hexcol, percent=50):
    """Gør en hex-farvekode mørkere."""
    rgb_ints = hexcol2ints(hexcol)
    removed_light = [int(i * percent / 100) for i in rgb_ints]
    darker_ints = [x[0] - x[1] for x in zip(rgb_ints, removed_light)]
    darker_hexcol = '#' + ''.join([hex(i)[2:].zfill(2) for i in darker_ints])
    return darker_hexcol


def code2colorcombo(spk, lttrcombs, colcombs, lttrs):
    """Hent en farvekombination til en talerkode. Udskift ukendte bogstaver med '_'."""
    spk = ''.join(['_' if l.upper() not in lttrs else l for l in spk])
    index = lttrcombs.index(spk)
    comb = colcombs[index]
    return comb


def make_chip_array(colordata, custom_xwidth=None):
    """Lav en HTML-side med alle talerkoders farvekombinationer -- i det x by y-layout der er tættest på 1:2."""
    print('Antal kombinationer der renderes på plakat:', len(colordata))
    n_combos = len(colordata)
    my_sqrt = math.ceil(math.sqrt(n_combos))

    xwidth, yheight = get_dimensions(n_combos, my_sqrt)
    print('Anbefalede dimensioner (ca. 1:2): xwith:', xwidth, 'yheight:', yheight)
    if custom_xwidth:
        xwidth = custom_xwidth
    # Generator der yield'er lister, hver liste har <xwidth> farvecombo-strenge (og bliver til en række).
    # zip_longest() sørger for at padde med None.
    rowdata_generator = itertools.zip_longest(*(iter(colordata),) * xwidth)
    rows = [make_row(rowdata_tuple) for rowdata_tuple in rowdata_generator]
    arraydiv = elem.DIV(elem.CLASS('chip_array'), *rows)
    return arraydiv


def get_dimensions(n, sq):
    """Find det x by y-layout der er tættest på 1:2."""
    _yheight, _xwidth = sq, sq
    for i in range(sq, 2 * sq):
        _yheight = i
        modulo = n % _yheight
        _xwidth = math.ceil(n / _yheight)
        if (_xwidth - modulo > 0) and (_xwidth - modulo < 10):
            if _yheight / _xwidth > 2:
                break
    return _xwidth, _yheight


def make_cell(combostring):
    """Lav en enkelt celle i farvechip-arrayen."""
    if combostring is None:
        return lxml.html.fromstring(f'<div class="cell"></div>')
    else:
        code, bg, border, fg = combostring.split('\t')
        style = f'color: {fg}; background: {bg}; border-color: {border};'
        cell = f'<div class="cell"><div class="inner" style="{style}">{code}</div></div>'
        return lxml.html.fromstring(cell)


def make_row(rowdata):
    """Lav en række af farvechip-arrayen."""
    cells = [make_cell(combostring) for combostring in rowdata]
    row = elem.DIV(elem.CLASS('row'), *cells)
    return row


def make_colcombo_html(colordata, title, strapline, html_path, custom_xwidth=None):
    """Lav farve-rendering af koderne i HTML."""
    html = elem.HTML(
        elem.HEAD(
            elem.META(charset="UTF-8"),
            elem.TITLE(title),
            elem.STYLE(STYLE)
        ),
        elem.BODY(
            make_chip_array(colordata, custom_xwidth),
            elem.H1(elem.CLASS("heading"), title),
            elem.P(strapline)
        )
    )

    html_with_doctype = f'<!DOCTYPE html>\n{lxml.html.tostring(html, pretty_print=True).decode("utf-8")}'
    with open(html_path, 'w', encoding='utf8') as out:
        out.write(html_with_doctype)


def main():
    tsv_path = 'output/colorcombos.tsv'
    html_path = 'output/colorcombos.html'
    make_colcombo_tsv(tsv_path)

    # Plakaten "LANCHART-talerne"
    with open(tsv_path, 'r', encoding='utf8') as infile:
        colordata_for_html = infile.read().splitlines()[1:]  # Skip headere.
    # Ekskluder koder med tal ...
    colordata_for_html = [x for x in colordata_for_html if not re.search(r'\d', x[:3])]
    title1 = "LANCHART-talerne"
    strapline1 = ("Samtlige 17.576 koder for nuværende og fremtidige talere i Sprogforandringscentrets korpus "
                  "— LANCHART-korpusset. De grå koder er interviewerkoder, der pr. konvention begynder med X.")
    make_colcombo_html(colordata_for_html, title1, strapline1, html_path, custom_xwidth=94)


if __name__ == '__main__':
    main()
